package com.example.applicationtest

import android.content.Context
import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView

class category_adapter(val context:Context,val data: ArrayList<datamodel>): RecyclerView.Adapter<category_adapter.category_holder>() {
    data class datamodel(val text: String)
    inner class category_holder(view: View):RecyclerView.ViewHolder(view){
        val text = view.findViewById<TextView>(R.id.category_text)
        val holder = view.findViewById<LinearLayout>(R.id.category_holder)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): category_holder {
        return category_holder(LayoutInflater.from(context).inflate(R.layout.category_list,parent,false))
    }

    override fun getItemCount(): Int {
        return data.size
    }

    override fun onBindViewHolder(holder: category_holder, position: Int) {
        val (text) = data[position]

        holder.text.setText(text)

        if(position==0){
            holder.holder.setBackgroundResource(R.drawable.blue_box)
            holder.text.setTextColor(Color.parseColor("#FFFFFFFF"))
        }
    }
}